module.exports = class Amount {
  constructor(value) {
    value = Number(value);
    this.validate(value);
    Object.freeze(this);
  }

  validate(value) {
    if (Number.isInteger(value) && !Number.isNaN(value) && value > 0 && value <= Number.MAX_SAFE_INTEGER) {
      this.value = value;
    } else {
      throw new TypeError();
    }
  }
}